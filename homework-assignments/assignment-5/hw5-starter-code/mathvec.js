"use strict";
var assert = require('assert');

var mathvec = function(entries) {
    // entries contains the values of the vector
    var that;
    // Inherit from Float64Array
    that = new Float64Array(entries);

    // Combine multiple arrays, grouping their corresponding elements
    function zip(arrays) {
        // Input: an Array of Arrays of length n
        // Output: an Array containing the grouped elements of each array
        // ex. output === [[arrays[0][0], arrays[1][0], arrays[2][0], ...],
        //                 [arrays[0][1], arrays[1][1], arrays[2][1], ...], ...
        //                 [arrays[0][n-1], arrays[1][n-1], arrays[2][n-1], ...]]
        return Array.apply(null,Array(arrays[0].length)).map(function(_,i){
            return arrays.map(function(array){return array[i]})
        });
    }

// Calculate the dot product of this and rhs
    that.dot = function(rhs) {
        // Input: a mathvec object
        // Output: the dot product of this and rhs if they are the
        //         same length
        // ex. output = sum(this[i] * rhs[i]) for all i
        
    };

// Calculate the sum of this and rhs
    that.plus = function(rhs) {
        // Input: a mathvec object
        // Output: a new mathvec object containing the sum of this and
        //         rhs if they are the same length
        // ex. output[i] = this[i] + rhs[i]
        
    };

// Calculate the difference of this and rhs
    that.minus = function(rhs) {
        // Input: a mathvec object
        // Output: a new mathvec object containing the difference of
        //         this and rhs if they are the same length
        // ex. output[i] = this[i] - rhs[i]
        

    };

// Perform scalar multiplication on this
    that.multiply = function(c) {
        // Input: a number
        // Output: a new mathvec object containing the product of the
        //         entries of this and c
        // ex. output[i] = this[i] * c
        
    };

// Calculate the inverse of this
    that.negate = function() {
        // Input: none
        // Output: a vector containing the inverse of this
        // ex. output[i] = -this[i]
        
    };


// Create a string representation of this
    that.toString = function() {
        // Input: none
        // Output: a string representation of this
        // ex. output = "[ " + this[0] + this[1] + ... + this[n] + "]"
        
    };

    // Place the values of the mathvec into a standard Array
    that.toArr = function() {
        // Input: none
        // Output: an Array object with the values of this
        // ex. var output = new Array(); output[i] = this[i];
        
    };


    return that;
};

var m=mathvec([1,2,3]), n=mathvec([4,5,6]);
console.log(m.dot(n));
console.log(m.plus(n));
console.log(m.minus(n));
console.log(m.negate());
console.log(m.toArr());
console.log(n.toString());
