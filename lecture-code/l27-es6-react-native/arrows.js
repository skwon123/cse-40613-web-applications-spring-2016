/**
 * Created by jesus on 4/25/16.
 */
"use strict";

var colors = require('colors/safe');

// how to write shorter functions using arrow functions

var a = ["Hydrogen", "Helium", "Lithium", "Beryllium"];
var b = ["H", "He", "Li", "Be"];

var a2 = a.map(function(s){ return s.length });

var a3 = a.map( s => s.length );

console.log(a2);
console.log(a3);

var zip = (a,b) => {var result = [];
    for (var i=0; i<a.length; i++) {
        var item = [a[i],b[i]];
        result.push(item);
    }
    return result;
};

console.log(zip(a,b));

var key_maker = val => ({key: val});

// example of how default binding of this fails in JS functions

function Prefixer(prefix) {
    this.prefix = prefix;
}
Prefixer.prototype.prefixArray = function (arr) { // (A)
    return arr.map(function (x) { // (B)
        // Doesn’t work:
        return this.prefix + x; // (C)
    });
};

var pre = new Prefixer("Hi ");
console.log(pre.prefixArray(['Jesus','Jeff','Zach']));

// Solution 1 in ES5: that = this

//function Prefixer(prefix) {
//    this.prefix = prefix;
//}
//Prefixer.prototype.prefixArray = function (arr) {
//    var that = this; // (A)
//    return arr.map(function (x) {
//        return that.prefix + x;
//    });
//};
//
//var pre = new Prefixer("Hi ");
//console.log(pre.prefixArray(['Jesus','Jeff','Zach']));

// Solution 2 in ES5: use bind

//function Prefixer(prefix) {
//    this.prefix = prefix;
//}
//Prefixer.prototype.prefixArray = function (arr) {
//    return arr.map(function (x) {
//        return this.prefix + x;
//    }.bind(this)); // (A)
//};
//
//var pre = new Prefixer("Hi ");
//console.log(pre.prefixArray(['Jesus','Jeff','Zach']));

// Solution in ES6: use arrow functions with lexical binding of this

//function Prefixer(prefix) {
//    this.prefix = prefix;
//}
//Prefixer.prototype.prefixArray = function (arr) {
//    return arr.map(x => this.prefix + x);
//};
//
//var pre = new Prefixer("Hi ");
//console.log(pre.prefixArray(['Jesus','Jeff','Zach']));

// Solution in ES6 also using classes

//class Prefixer {
//    constructor(prefix) {
//        this.prefix = prefix;
//    }
//    prefixArray(arr) {
//        return arr.map(x => this.prefix + x);
//    }
//    static get defaultPrefix() {
//        return new Prefixer("Hello ");
//    }
//}
//
//var pre = new Prefixer("Hi ");
//console.log(pre.prefixArray(['Jesus','Jeff','Zach']));
//// call static getter
//pre = Prefixer.defaultPrefix;
//console.log(pre.prefixArray(['Jesus','Jeff','Zach']));
//
//// Inheritance
//
//class CPrefixer extends Prefixer{
//    constructor(prefix,color) {
//        super(prefix);
//        this.color = color;
//    }
//    printColor(arr) {
//        arr=super.prefixArray(arr);
//        arr=arr.join(', ');
//        eval("console.log(colors."+this.color+"(arr))");
//    }
//}
//
//var cpre = new CPrefixer("Hi ","blue");
//cpre.printColor(['Jesus','Jeff','Zach']);
