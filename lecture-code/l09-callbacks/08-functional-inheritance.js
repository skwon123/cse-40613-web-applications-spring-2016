/**
 * Created by jesus on 2/8/16.
 */
"use strict";

// Here we implement functional inheritance with public data member vertices

function polygon(vertices) {
    return {
        vertices: vertices,
        order: function () {
            return this.vertices.length;
        },
        perimeter: function () {
            var result = 0, order = this.order(), i, a, b, xd, yd;
            for (i = 0; i < this.order(); i++) {
                a = this.vertices[i % order];
                b = this.vertices[(i + 1) % order];
                xd = a.x - b.x;
                yd = a.y - b.y;
                result += Math.sqrt(xd * xd + yd * yd);
            }
            return result;
        }
    };
}

var myPolygon =  polygon([
    {x: 0, y: 0},
    {x: 1, y: 0},
    {x: 1, y: 1},
    {x: 0, y: 1}]);

function square(vertices) {
    var that = polygon(vertices);
    that.perimeter = function() {
        var a, b, xd, yd;
        a = this.vertices[0];
        b = this.vertices[1];
        xd = a.x - b.x;
        yd = a.y - b.y;
        return 4*(Math.sqrt(xd*xd+yd*yd));
    };
    return that;
}

var mySquare = square([
    {x: 0, y: 0},
    {x: 2, y: 0},
    {x: 2, y: 2},
    {x: 0, y: 2}]);

console.log(mySquare.perimeter());

// Here we implement inheritance with private data member vertices

function polygon2(vertices) {
    return {
        order: function () {
            return vertices.length;
        },
        perimeter: function () {
            var result = 0, order = this.order(), i, a, b, xd, yd;
            for (i = 0; i < this.order(); i++) {
                a = vertices[i % order];
                b = vertices[(i + 1) % order];
                xd = a.x - b.x;
                yd = a.y - b.y;
                result += Math.sqrt(xd * xd + yd * yd);
            }
            return result;
        }
    };
}

function square2(vertices) {
    var that = polygon(vertices);
    that.perimeter = function() {
        var a, b, xd, yd;
        a = vertices[0];
        b = vertices[1];
        xd = a.x - b.x;
        yd = a.y - b.y;
        return 4*(Math.sqrt(xd*xd+yd*yd));
    };
    return that;
}

var mySquare2 = square2([
    {x: 0, y: 0},
    {x: 2, y: 0},
    {x: 2, y: 2},
    {x: 0, y: 2}]);

console.log(mySquare2.perimeter());