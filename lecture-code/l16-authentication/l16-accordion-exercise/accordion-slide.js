/* Jesus A. Izaguirre, March 3, 2016 
   Solution to L16 accordion exercise
   Web Apps Spring 2016              */
"use strict";

$(function() {
    $('#accordion').on('click', '.accordion-block', function(e){ // When clicked
        e.preventDefault();                    // Prevent default action of button
        $(this)                                // Get the element the user clicked on
            .children('.accordion-block-body')            // Select child panel
            .not(':animated')                    // If it is not currently animating
            .slideToggle();                      // Use slide toggle to show or hide it
    });
});

