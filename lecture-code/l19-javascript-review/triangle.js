"use strict";

// Check to see if the obj contains a property
// with key prop_name in its own prototype
var hasProp;

// Create a Triangle object that accepts an array of three (x,y)
// points and a name, and which has the following methods:
//      Triangle.perimeter - calculate and return the perimeter
//      Triangle.area - calculate and return the area
//      Triangle.hasPoint - find an (x,y) point in the triangle,
//                          return the point is it is in the triangle
//                          and false otherwise
//      Triangle.sortPoints - accepts and custom comparison function
//                            and sorts the points in the triangle by
//                            that function, otherwise sorts based on
//                            proximity to the origin
//      Triangle.print - log the name of the triangle and each point
//                       to the console
var Triangle;

// Create an array containing the points (0,0), (1,1), and (1,0)
var my_points;

var tri;


// Store a function inside of this variable that accepts two
// points and returns true if the first is closer to the origin
// than the second
var my_comparator;

// Display the triangle


// Calculate and print the perimeter


// Calculate and print the area


// Find the point (3,7)


// Find the point (1,0)


// Sort using the default comparator and print


// Sort using the my_comparator function and print


// Sort using an anonymous function and print
