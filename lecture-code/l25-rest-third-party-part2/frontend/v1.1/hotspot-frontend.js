/**
 * Created by jesus on 4/12/16.
 */
"use strict";

$(function () {
    var url = "https://ec2-54-89-76-246.compute-1.amazonaws.com/api/hotspots/?limit=15&near=new%20york%20city";
    var venues = $(".links-list"), item;
    // code to be able to use Asignio authentication
    var asignioToken = "6fe0724d2800e611bceb0a9d54ca7dfd";
    AsignioAuth.initialize({
        applicationToken: asignioToken,
        containerID: "asignio"
    });
    // get hotspot data from hotspots server
    $.getJSON(url)
        .done(function (data) {
            console.log("hotspots returned:", data);
            data.forEach(function (venue) {
                console.log("venue", venue);
                item = '<li class="list-group-item link">'
                    + venue.name
                    + '<span class="badge">'
                    + venue.votes
                    + '</span>';
                venues.append($(item));
            })
        })
        .fail(function (err) {
            console.log("hotspots failed:", err);
        });
});